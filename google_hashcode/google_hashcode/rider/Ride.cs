﻿using System;
using System.Collections.Generic;
using System.Text;

namespace google_hashcode.rider
{
    class Ride:IComparable<Ride>

    {
    private Tuple<int, int> start, end;
    private int id, earliest, latest, distance;
    public bool IsTaken { get; set; }

    public Tuple<int, int> Start => start;

    public Tuple<int, int> End => end;

    public int Id1 => id;

    public int Earliest => earliest;

    public int Latest => latest;

    public int Distance => distance;

    public Ride(string input, int id)
    {
        ParseInput(input);
        this.id = id;
        IsTaken = false;
        distance = Utils.CalcDistance(start, end);
    }

    private void ParseInput(string input)
    {
        string[] split = input.Split(' ');
        start = new Tuple<int, int>(Int32.Parse(split[0]), Int32.Parse(split[1]));
        end = new Tuple<int, int>(Int32.Parse(split[2]), Int32.Parse(split[3]));
        earliest = Int32.Parse(split[4]);
        latest = Int32.Parse(split[5]);
    }

    public int Id => id;


        public int CompareTo(Ride other)
        {
            int result = 0;
            if (result == 0) result = -Earliest.CompareTo(other.Earliest);
            return result;
        }
    }
}
