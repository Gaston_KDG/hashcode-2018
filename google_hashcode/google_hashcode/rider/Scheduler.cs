﻿using System;
using System.Collections.Generic;
using System.Text;

namespace google_hashcode.rider
{
    class Scheduler
    {
        int rows, cols, vehicleCount, rideCount, bonus, steps;
        List<Ride> freeRides;
        List<Ride> takenRides;
        private List<Vehicle> vehicles;


        public Scheduler(List<string> input)
        {
            ParseInput(input);
            Schedule();
        }

        private void Schedule()
        {
            //Closest();
            //ByCost();
            //ByCostInverted();//32M
            VehiclesByCostEqually();//37 M
        }

        private void VehiclesByCostEqually()
        {
            //List<Vehicle> vehicleList = new List<Vehicle>(vehicles);
            bool rideAssigned;
            do
            {
                rideAssigned = false;
                foreach (var vehicle in vehicles)
                {
                    Ride chosenRide = null;
                    int rideIndex = -1;
                    int cost = Int32.MaxValue;

                    for (int i = freeRides.Count - 1; i >= 0; i--)
                    {
                        Ride r = freeRides[i];
                        if (vehicle.IsOverdue(r)) continue;
                        if (vehicle.CalcCost(r) > vehicle.Steps) continue;
                        //int c = vehicle.CalcCost(r, true);
                        int c = vehicle.WastedCost(r);

                        if (chosenRide == null || c <= cost)
                        {
                            if (c==cost && vehicle.CalcCost(r, true) > vehicle.CalcCost(chosenRide,true)) continue;//give priority to shorter rides
                            chosenRide = r;
                            rideIndex = i;
                            cost = c;
                        }
                    }
                    if (chosenRide == null) continue;  //no ride fits in vehicle, move to next

                    vehicle.AssignRide(chosenRide);
                    takenRides.Add(chosenRide);
                    freeRides.RemoveAt(rideIndex);
                    rideAssigned = true;
                }
            } while (rideAssigned);
            Console.WriteLine("Free rides left: " + freeRides.Count);
        }

        private void ByCostInverted()
        {
            // method should loop over the vehicles and assign each vehicle the best possible route
            foreach (var vehicle in vehicles)
            {
                VehicleRideComparer comp = new VehicleRideComparer(vehicle);
                while (vehicle.HasStepsLeft())
                {
                    //freeRides.Sort(comp);
                    Ride chosenRide = null;
                    int rideIndex = -1;
                    int cost = Int32.MaxValue;

                    for (int i = freeRides.Count - 1; i >= 0; i--)
                    {
                        Ride r = freeRides[i];
                        if (vehicle.IsOverdue(r)) continue;
                        if (vehicle.CalcCost(r) > vehicle.Steps) continue;
                        int c = vehicle.CalcCost(r, true);
                        
                        if (chosenRide == null || c < cost)
                        {
                            chosenRide = r;
                            rideIndex = i;
                            cost = c;
                        }
                    }
                    if (chosenRide == null) break;  //no ride fits in vehicle, move to next

                    vehicle.AssignRide(chosenRide);
                    takenRides.Add(chosenRide);
                    freeRides.RemoveAt(rideIndex);
                }
            }
            Console.WriteLine("Free rides left: " + freeRides.Count);
        }

        private void ByCost()
        {

            freeRides.Sort(new RideComparer());
            for (int i = freeRides.Count - 1; i >= 0; i--)
            {
                Ride currentRide = freeRides[i];
                Vehicle bestVehicle = null;
                int cost = 0;
                foreach (var vehicle in vehicles)
                {
                    if (vehicle.IsOverdue(currentRide)) continue;
                    if (vehicle.CalcCost(currentRide) > vehicle.Steps) continue;
                    //int c = vehicle.CalcCost(currentRide, true);
                    int c = vehicle.WastedCost(currentRide);
                    
                    if (bestVehicle == null || c <= cost)
                    {
                        bestVehicle = vehicle;
                        cost = c;
                    }
                }

                if (bestVehicle == null) continue;
                bestVehicle.AssignRide(currentRide);
                takenRides.Add(currentRide);
                freeRides.RemoveAt(i);
            }
            Console.WriteLine("Free rides left: " + freeRides.Count);
        }

        private void Closest()
        {
            freeRides.Sort(new RideComparer());
            for (int i = freeRides.Count - 1; i >= 0; i--)
            {
                Vehicle closest = null;
                int distance = 0;
                foreach (var vehicle in vehicles)
                {
                    int dist;
                    if (closest == null)
                    {
                        dist = Utils.CalcDistance(vehicle.Position, freeRides[i].Start);
                        if ((dist + freeRides[i].Distance) > vehicle.Steps) continue;

                        closest = vehicle;
                        distance = dist;
                    }

                    dist = Utils.CalcDistance(vehicle.Position, freeRides[i].Start);

                    if ((dist + freeRides[i].Distance) > vehicle.Steps) continue;

                    if (dist < distance)
                    {
                        closest = vehicle;
                        distance = dist;
                    }
                }

                if (closest == null) continue;
                closest.AssignRide(freeRides[i]);
                takenRides.Add(freeRides[i]);
                freeRides.RemoveAt(i);
            }
            Console.WriteLine("Free rides left: " + freeRides.Count);
        }

        private void ParseInput(List<string> input)
        {
            string[] header = input[0].Split(' ');
            rows = Int32.Parse(header[0]);
            cols = Int32.Parse(header[1]);
            vehicleCount = Int32.Parse(header[2]);
            rideCount = Int32.Parse(header[3]);
            bonus = Int32.Parse(header[4]);
            steps = Int32.Parse(header[5]);
            freeRides = new List<Ride>();
            takenRides = new List<Ride>();
            vehicles = new List<Vehicle>();

            for (int i = 0; i < vehicleCount; i++)
            {
                vehicles.Add(new Vehicle(i, steps,bonus));
            }

            for (int i = 1; i < 1 + rideCount; i++)
            {
                freeRides.Add(new Ride(input[i], i - 1));
            }
        }

        public List<string> Output()
        {
            List<string> vehicleOut = new List<string>();
            foreach (var vehicle in vehicles)
            {
                vehicleOut.Add(vehicle.ToString());
            }

            return vehicleOut;
        }

        public long Score()
        {
            long score = 0;
            foreach (var vehicle in vehicles)
            {
                /*
                int step = 0;
                List<Ride> rides = new List<Ride>(vehicle.Rides);
                //rides.Sort();
                foreach (var ride in rides)
                {
                    step += vehicle.WastedCost(ride);//add transit and waiting time
                    int end = step += ride.Distance; //calc end of ride
                    if (end <= ride.Latest)
                    {
                        String s = "Ride, score: " + ride.Distance;
                        score += ride.Distance;
                        if (step == ride.Earliest)
                        {
                            s = s + " bonus: " + bonus;
                            score += bonus;
                        }
                        Console.WriteLine(s);
                    }
                    step = end;
                }
                */
                score += vehicle.Score;
            }
            return score;
        }
    }
}
