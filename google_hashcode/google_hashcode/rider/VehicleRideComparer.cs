﻿using System;
using System.Collections.Generic;
using System.Text;

namespace google_hashcode.rider
{
    class VehicleRideComparer:IComparer<Ride>
    {
        private Vehicle vehicle;

        public VehicleRideComparer(Vehicle vehicle)
        {
            this.vehicle = vehicle;
        }

        public int Compare(Ride x, Ride y)
        {
            int distX, distY,result = 0;
            distX = Utils.CalcDistance(vehicle.Position, x.Start);
            distY = Utils.CalcDistance(vehicle.Position, y.Start);

            if (result == 0) result = distX.CompareTo(distY);
            if (result == 0) result = x.Distance.CompareTo(y.Distance);
            if (result == 0) result = x.Earliest.CompareTo(y.Earliest);

            return result;
        }
    }
}
