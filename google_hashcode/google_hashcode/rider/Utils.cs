﻿using System;
using System.Collections.Generic;
using System.Text;

namespace google_hashcode.rider
{
    class Utils
    {
        private Utils()
        {
        }


        public static int CalcDistance(Tuple<int, int> start, Tuple<int, int> end)
        {
            int distance = 0;

            distance += Math.Abs(start.Item1 - end.Item1);
            distance += Math.Abs(start.Item2 - end.Item2);
            return distance;
        }
    }
}
