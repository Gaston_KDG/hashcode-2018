﻿using System;
using System.Collections.Generic;
using System.Text;

namespace google_hashcode.rider
{
    class Transit
    {
        private Tuple<int, int> start, end;
        private int distance;

        public Transit(Tuple<int, int> start, Tuple<int, int> end)
        {
            this.start = start;
            this.end = end;
            this.distance = Utils.CalcDistance(start, end);
        }
    }
}
