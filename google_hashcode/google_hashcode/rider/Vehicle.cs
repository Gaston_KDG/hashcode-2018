﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection.Metadata;
using System.Text;

namespace google_hashcode.rider
{
    class Vehicle
    {
        private readonly int _bonus;
        public int Id { get; }
        public int InitialSteps { get; private set; }
        public int Steps { get; private set; }
        public int StepsTaken { get; private set; }
        public List<Ride> Rides { get; }
        public Tuple<int, int> Position { get; set; }
        public int Score { get; private set; }

        public Vehicle(int id, int steps, int bonus)
        {
            _bonus = bonus;
            this.Id = id;
            Score = 0;
            Steps = steps;
            InitialSteps = Steps;
            StepsTaken = 0;
            this.Rides = new List<Ride>();
            Position = new Tuple<int, int>(0, 0);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Rides.Count).Append(" ");

            foreach (var ride in Rides)
            {
                sb.Append(ride.Id + " ");
            }

            return sb.ToString();
        }

        public void AssignRide(Ride ride)
        {
            Rides.Add(ride);
            int cost = CalcCost(ride);
            Steps -= cost;

            StepsTaken += WastedCost(ride);

            if (StepsTaken == ride.Earliest)
            {
                Score += _bonus;
            }
            StepsTaken += ride.Distance;
            Score += ride.Distance;


            Position = ride.End;
            //StepsTaken += CalcCost(ride);
            if (Steps < 0)
            {
                Console.WriteLine("ERROR " + Steps);
            }
        }

        public int CalcCost(Ride ride, bool addTimeOverdue = false)
        {
            int cost = 0;
            cost += Utils.CalcDistance(Position, ride.Start);
            int stepAtPoint = StepsTaken + cost;
            int waitTime = (ride.Earliest - stepAtPoint);
            if (waitTime > 0)
            {
                cost += waitTime;
            }
            else
            {
                if (addTimeOverdue)
                {
                    cost += Math.Abs(waitTime);
                }
            }
            cost += ride.Distance;

            if (addTimeOverdue)
            {
                int timeOverdue = 0;
                int stepAtEnd = StepsTaken + cost;
                timeOverdue = stepAtEnd - ride.Latest;
                if (timeOverdue > 0)
                {
                    cost += (timeOverdue * 8);
                }
                else
                {
                    cost += Math.Abs(timeOverdue);
                }
            }

            return cost;
        }
        public int WastedCost(Ride ride)
        {
            int cost = 0;
            cost += Utils.CalcDistance(Position, ride.Start);
            int stepAtPoint = StepsTaken + cost;
            int waitTime = (ride.Earliest - stepAtPoint);
            if (waitTime > 0)
            {
                cost += waitTime;
            }
            return cost;
        }
        public bool IsOverdue(Ride ride)
        {
            int timeOverdue = 0;
            int stepAtEnd = StepsTaken + CalcCost(ride);
            stepAtEnd += 1;
            timeOverdue = stepAtEnd - ride.Latest;
            if (timeOverdue > 0)
            {
                return true;
            }
            return false;
        }

        public bool HasStepsLeft()
        {
            return InitialSteps > StepsTaken;
        }
    }
}
