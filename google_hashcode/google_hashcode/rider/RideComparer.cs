﻿using System;
using System.Collections.Generic;
using System.Text;

namespace google_hashcode.rider
{
    class RideComparer : IComparer<Ride>
    {
        public int Compare(Ride x, Ride y)
        {
            int result = 0;
            if (result == 0) result = -x.Latest.CompareTo(y.Latest);
            if (result == 0) result = -x.Earliest.CompareTo(y.Earliest);
            if (result == 0) result = -x.Distance.CompareTo(y.Distance);
            return result;
        }
    }
}
