﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using google_hashcode.io;
using google_hashcode.rider;

namespace google_hashcode
{
    class Program
    {
        /// <summary>
        /// Entry point of the program
        /// This includes running all data sets and timing their execution time
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var totalTimer = new Stopwatch();
            var watch = new Stopwatch();
            watch.Start();
            watch.Stop();
            Thread.Sleep(1000);
            watch.Restart();
            totalTimer.Start();

            var scoreA = RunA();

            watch.Stop();
            Console.WriteLine("A: " + watch.ElapsedMilliseconds + " ms");
            watch.Restart();

            var scoreB = RunB();

            watch.Stop();
            Console.WriteLine("B: " + watch.ElapsedMilliseconds + " ms");
            watch.Restart();

            var scoreC = RunC();

            watch.Stop();
            Console.WriteLine("C: " + watch.ElapsedMilliseconds + " ms");
            watch.Restart();

            var scoreD = RunD();

            watch.Stop();
            Console.WriteLine("D: " + watch.ElapsedMilliseconds + " ms");
            watch.Restart();

            var scoreE = RunE();

            watch.Stop();
            totalTimer.Stop();
            Console.WriteLine("E: " + watch.ElapsedMilliseconds + " ms");

            Console.WriteLine("Total Score: " + (scoreA + scoreB + scoreC + scoreD + scoreE));
            Console.WriteLine("Total Time: " + totalTimer.Elapsed.TotalSeconds + "s");


            Console.WriteLine("Press a key to continue...");
            Console.ReadKey();
        }

        private static long RunE()
        {
            List<string> lines = Reader.ReaderIn(Reader.E);
            Scheduler s = new Scheduler(lines);
            Writer.WriteLines(Writer.E, s.Output());
            Console.WriteLine("Score E: " + s.Score());
            return s.Score();
        }

        private static long RunD()
        {
            List<string> lines = Reader.ReaderIn(Reader.D);
            Scheduler s = new Scheduler(lines);
            Writer.WriteLines(Writer.D, s.Output());
            Console.WriteLine("Score D: " + s.Score());
            return s.Score();
        }

        private static long RunC()
        {
            List<string> lines = Reader.ReaderIn(Reader.C);
            Scheduler s = new Scheduler(lines);
            Writer.WriteLines(Writer.C, s.Output());
            Console.WriteLine("Score C: " + s.Score());
            return s.Score();
        }

        private static long RunB()
        {
            List<string> lines = Reader.ReaderIn(Reader.B);
            Scheduler s = new Scheduler(lines);
            Writer.WriteLines(Writer.B, s.Output());
            Console.WriteLine("Score B: " + s.Score());
            return s.Score();
        }

        private static long RunA()
        {
            List<string> lines = Reader.ReaderIn(Reader.A);
            Scheduler s = new Scheduler(lines);
            Writer.WriteLines(Writer.A, s.Output());
            Console.WriteLine("Score A: " + s.Score());
            return s.Score();
        }
    }
}