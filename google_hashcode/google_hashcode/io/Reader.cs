﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace google_hashcode
{
	class Reader
	{
		public const string A = "a_example.in";
		public const string B = "b_should_be_easy.in";
		public const string C = "c_no_hurry.in";
        public const string D = "d_metropolis.in";
        public const string E = "e_high_bonus.in";

        private Reader()
		{
		}

		public static List<string> ReaderIn(String fileName)
		{
			string path = Path.Combine(Environment.CurrentDirectory, @"files\in\", fileName);
			List<string> lines = new List<string>();
			try
			{   // Open the text file using a stream reader.
				using (StreamReader sr = new StreamReader(path))
				{
					// Read the stream to a string, and write the string to the console.
					while (!sr.EndOfStream)
					{
						string s = sr.ReadLine();
						lines.Add(s);
					}
				}
			} catch (Exception e)
			{
				Console.WriteLine("The file could not be read:");
				Console.WriteLine(e.Message);
				throw new Exception("something went wrong so we crash the place");
			}
			return lines;
		}
	}
}
