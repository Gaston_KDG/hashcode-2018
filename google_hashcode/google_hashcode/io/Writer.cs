﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace google_hashcode.io
{
	class Writer
    {
        public const string A = "a_example.out";
        public const string B = "b_should_be_easy.out";
        public const string C = "c_no_hurry.out";
        public const string D = "d_metropolis.out";
        public const string E = "e_high_bonus.out";


        private Writer()
		{
		}
		
		public static void WriteLines(string fileName, List<string> lines)
		{
			var directory = Path.Combine(Environment.CurrentDirectory, @"files\out\");
			var path = Path.Combine(directory, fileName);
			try
			{
				if (File.Exists(path))
				{
					File.Delete(path);
				}

				Directory.CreateDirectory(directory);

				using (StreamWriter file = new StreamWriter(path))
				{
					foreach (string line in lines)
					{
						file.WriteLine(line);
					}
				}
			} catch (Exception e)
			{
				Console.WriteLine("The file could not be written:");
				Console.WriteLine(e.Message);
				throw new Exception("something went wrong so we crash the place");
			}
		}
	}
}
